package LO1.num;

// class begins a class declaration 
public class FindEvenOddNumber {

	// main method begins execution of java application
	public static void main (String args []) {
		
		// declare veriable N is a data of type int 
		int N = 99;
	
		// reminder modulate by 2
		int reminder = N % 2 ;
		
				// if reminder 0, print n is a even number
				if (reminder == 0) {
					System.out.println ( N +" is a Even Number!");
					
				// else reminder not equal 0, print N is a odd number
				} else {
					System.out.println ( N + " is a Odd Number!");	
					
					
				} // end if statment
				
	} // end main method
	
} // end class FindEvenOddNumber


/*
output
99 is a Odd Number!

*/