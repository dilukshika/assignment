package LO3.Grade.stu;

public class StudentDetails{
	
	public static final String schoolName = "J/ CHAVAKACHCHERI HINDU COLLEGE";
	protected  String student_Name;
	private  int student_ID;
    private  int mathematics;
	private  int chemistry;
    private  int physics;
    private String chemistryGrading;
    private String mathematicsGrading;
    private String physicsGrading;
	
	
	
	public void studentDetail(String student_Name,int student_ID, int  mathematics, int chemistry, int physics ) {
		
		this.student_Name = student_Name;
		this.student_ID = student_ID;
        this.mathematics = mathematics;
        this.chemistry = chemistry;
        this.physics = physics;
       
	}

	    public int getChemistry(int chemistry) {
	        return chemistry;
	    }

	    public int getMathematics() {
	        return mathematics;
	    }

	    public int getPhysics() {
	        return physics;
	    }
	
	  
	    public void chemistryGrading() {
	    	
	    	if(this.chemistry > 0 && this.chemistry <= 100)
	    		if(this.chemistry >= 75){
	    			chemistryGrading = " A ";
	    		}else if(this.chemistry >= 60){
	    			chemistryGrading = " B ";
	    		}else if(this.chemistry >= 45){
	    			chemistryGrading = " C ";
	    		}else {
					chemistryGrading = " D ";
	    		}
				System.out.println(chemistryGrading);
	    }
	    
	    public String getChemistryGrading() {
	    	return chemistryGrading;
	    }
	   
	    
	    public void mathematicsGrading() {
	    	
	    	if(this.mathematics > 0 && this.mathematics <= 100)
	    		if(this.mathematics >= 75){
					mathematicsGrading = " A ";
	    		}else if(this.mathematics >= 60){
					mathematicsGrading = " B ";
	    		}else if(this.mathematics >= 45){
					mathematicsGrading = " C ";
	    		}else {
					mathematicsGrading = " D ";
	    		}
				System.out.println(mathematicsGrading);
	    }
	    
	    public String getMathematicsGrading() {
	    	return mathematicsGrading;
	    }
	    
	 
	    
	    public void physicsGrading() {
	    	
	    	if(this.physics > 0 && this.physics <= 100)
	    		if(this.physics >= 75){
					physicsGrading=" A ";
	    		}else if(this.physics >= 60){
					physicsGrading=" B ";
	    		}else if(this.physics >= 45){
					physicsGrading=" C ";
	    		}else {
					physicsGrading=" D ";
	    		}
				System.out.println(physicsGrading);
	    }
   
	   public String getPhysicsGrading() {
	    	return physicsGrading;
   }
   
		public  void get_Student_Details() {
			System.out.println("_____________________________________________");
		 	System.out.println("SCHOOL NAME: " + schoolName);
		 	System.out.println("STUDENT NAME: " + student_Name);
		 	System.out.println("STUDENT ID: " + student_ID);
		 	
		 	System.out.println("  Chemistry Marks: " + chemistry);
		 	System.out.println("  Chemistry Grade: " + chemistryGrading);
		 	
		 	System.out.println("  Mathematics Marks: " + mathematics);
		 	System.out.println("  Mathematics Grade: " + mathematicsGrading );
		 	
		 	System.out.println("  Physics Marks: " + physics);
		 	System.out.println("  Physics Grade: " + physicsGrading );
		 	System.out.println("_____________________________________________");
		}
}