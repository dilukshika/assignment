package LO3.Grade.stu;

public class StudentDetailsDemo {
	

	public static  void main (String args []) {
	
	StudentDetails tom = new StudentDetails();
	StudentDetails rome = new StudentDetails();
	StudentDetails simon = new StudentDetails();
	StudentDetails darek = new StudentDetails();
	
    
    tom.studentDetail("Tom", 1, 85, 45, 65);
    rome.studentDetail("Rome", 2, 55, 75, 65);
    simon.studentDetail("simon", 3, 85, 95, 52);
    darek.studentDetail("Darek", 4,35,65,45);
    
    
    tom.chemistryGrading();
    rome.chemistryGrading();
    simon.chemistryGrading();
    darek.chemistryGrading();
   

    tom.mathematicsGrading();
    rome.mathematicsGrading();
    simon.mathematicsGrading();
    darek.mathematicsGrading();
    
 
    
    tom.get_Student_Details();
    rome.get_Student_Details();
    simon.get_Student_Details();
    darek.get_Student_Details();
    
    
}	
	
}

