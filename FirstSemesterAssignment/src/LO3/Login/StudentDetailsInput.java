package LO3.Login;
import java.util.Scanner;

/**
 * Students details input here
 * @author Dilukshika
 * @version 1.1.0v
 * @since 2020/06/01
 * 
 */

public class StudentDetailsInput{
	
	public static final String schoolName = "J/ CHAVAKACHCHERI HINDU COLLEGE";
    private static int n;
    private static final String batch = "2018";
    
    
	public static void main(String args []) {
		
		LoginSystem login = new LoginSystem();
		if(login.isValidLogin()){
			StudentDetailsInput studentDetailsInput = new StudentDetailsInput();
			studentDetailsInput.getMarkes();
		}

	}

	public void getMarkes(){
		String [] student_Name = new String [3];
		String [] subject = {"Maths", "Chemistry", "Physics"};

		int [] marks = new int [3];


		int total = 0;
		int average = 0;

		Scanner scan = new Scanner (System.in);

		for(int n=0; n < student_Name.length; n++) {

			System.out.println("Please Enter Student Name? ");
			student_Name [n] = scan.nextLine();

			for (int i=0; i< student_Name.length; i++) {
				System.out.println("Please Enter " + subject[i] + " Marks? ");
				marks[i]=scan.nextInt();
				total = total + marks[i];

			}


			average = total / student_Name.length;

			System.out.println("__________________________________________________________________________________________________________________________________________________________________________________");
			System.out.println(" \t\t\t\t School Name: " +  " ****** " +schoolName + " ****** " );
			System.out.println(" \t\t\t\t Batch: " + " ***** "+ batch + " ***** ");
			System.out.println(" Student Name: \t\t " +student_Name [0] );
			System.out.println(" Maths Marks: \t\t " + marks[0]);
			System.out.println(" Chemistry Marks: \t " + marks[1]);
			System.out.println(" Physics Marks: \t " + marks[2]);
			System.out.println(" Total Marks: \t\t " +  total);
			System.out.println(" Average: \t\t " + average);
			System.out.println("__________________________________________________________________________________________________________________________________________________________________________________");
		}
	}
}

	
	
	
	
