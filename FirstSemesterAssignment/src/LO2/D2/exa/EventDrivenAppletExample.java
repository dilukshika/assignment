package LO2.D2.exa;

//Java packages
import java.awt.Graphics; // import class Graphics
import javax.swing.JApplet; //import class JApplet


public class EventDrivenAppletExample extends JApplet {
	
	//draw text on applet's background
	public void paint(Graphics g) {
		
		//call superclass version of method paint
		super.paint(g);
		
		//draw a String at x-coordinate 25 and y- coordinate 25
		g.drawString("WelCome to Dilukshika's Assignment!!!!!!!! ", 25, 25);
		
		
	}// end method paint
}//end class EventDrivenAppletExample

