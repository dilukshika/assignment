package LO2.D2.exa;

	//Superclass 
	class Principal {
		String designation = "Principal";
		String SchoolName = "J/Chavakachcheri Hidu College";
		
		void Principal_Details() {
			System.out.println("Principal is a superclass.");
	 }
	}
	
	//Subclass
	class HeadMaster extends Principal {
		void HeadMaster_Details() {
			System.out.println("HeadMaster is a Subclass.");
	 }
	}

public class OOPInheritanceExample {
		
	 public static void main(String args[]) {
		 HeadMaster s = new HeadMaster();
		 System.out.println(s.SchoolName);
		 System.out.println(s.designation);
	  s.Principal_Details();
	  s.HeadMaster_Details();
	 }
	}

