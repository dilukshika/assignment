class student {
    // Declearing the arrays of records called students
    String[] Name = new String[100];
    int[] StudentNumber = new int[100];
    int[] ChemistryMarks = new int[100];
    int[] MathsMarks = new int[100];
    int[] PhysicsMarks = new int[100];
}

public class StudentDetailsInput{
	
	public static final String schoolName = "J/ CHAVAKACHCHERI HINDU COLLEGE";
    private static int n;
    private static final String batch = "2018";
    static int i = 0;
    
	public static void main(String args []) {
	    
	    
		student allStudents = new student();    // create total student records maximum is 100 
		
		
		// Manage student records from here 
		allStudents.Name[i] = "Gunalan Sivanathan";
		allStudents.StudentNumber[i] = 12345;
		allStudents.ChemistryMarks[i] = 10;
		allStudents.MathsMarks[i] = 30;
		allStudents.PhysicsMarks[i] = 40;
        System.out.println("Student Name is : " + allStudents.Name[i]);  // print simple message 
        System.out.println("\nStudent Number is : " + allStudents.StudentNumber[i]);  // print simple message 
        System.out.println("\nStudent Chemistry marks is : " + allStudents.ChemistryMarks[i]);  // print simple message 
        System.out.println("\nStudent Maths marks is : " + allStudents.MathsMarks[i]);  // print simple message 
        System.out.println("\nStudent Physics marks is : " + allStudents.PhysicsMarks[i]);  // print simple message 
        System.out.println("\n\n  *****  End of 1st record   ***** \n\n\n\n ");  // print simple
        i = i+1;   // this will increment the arrray index by 1, prepare for next record
        // lets try to record 2nd student record 
        
        allStudents.Name[i] = "Dilukshi Sivanathan";
		allStudents.StudentNumber[i] = 23567;
		allStudents.ChemistryMarks[i] = 55;
		allStudents.MathsMarks[i] = 35;
		allStudents.PhysicsMarks[i] = 75;
        System.out.println("Student Name is : " + allStudents.Name[i]);    // print simple message 
        System.out.println("\nStudent Number is : " + allStudents.StudentNumber[i]);    // print simple message 
        System.out.println("\nStudent Chemistry marks is : " + allStudents.ChemistryMarks[i]);    // print simple message 
        System.out.println("\nStudent Maths marks is : " + allStudents.MathsMarks[i]);    // print simple message 
        System.out.println("\nStudent Physics marks is : " + allStudents.PhysicsMarks[i]);    // print simple message
        System.out.println("\n\n  *****  End of 2nd record   ***** \n\n\n\n ");    // print simple
        i = i+1;     // we can increment this untill the index 99where the total number of records are 100(0 to 99)
                    // let see if we could print the 1st record again to verify the record entered is not overwritten !!!!
          
		  
        i = 0;  // this should call the 1st record we entered where is "Gunalan" record  !!
               // print the record again 
        System.out.println("Student Name is : " + allStudents.Name[i]);  // print simple message 
        System.out.println("\nStudent Number is : " + allStudents.StudentNumber[i]);  // print simple message 
        System.out.println("\nStudent Chemistry marks is : " + allStudents.ChemistryMarks[i]);  // print simple message 
        System.out.println("\nStudent Maths marks is : " + allStudents.MathsMarks[i]);  // print simple message 
        System.out.println("\nStudent Physics marks is : " + allStudents.PhysicsMarks[i]);  // print simple message
        System.out.println("\n\n  *****  End of 1st record   ***** \n\n\n\n ");  // print simple
        
        // did you noticed that the printing the record code or System.Out.println has not changed and it is repeting only thing differ is the value of i !! this way you can manage as many as records you want as long as your record is within 100. 
	}
}
	
	
